package com.AndriFrolov.Dictionary;
//
//import com.AndriFrolov.Dictionary.Model.WordAndTranslations;
//import com.AndriFrolov.Dictionary.Repository.EnglishDictionaryRepository;
//import com.AndriFrolov.Dictionary.Service.EnglishDictionaryService;
//import static org.hamcrest.Matchers.is;
//import static org.mockito.Mockito.doReturn;
//import static org.mockito.Mockito.any;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.springframework.test.web.servlet.MockMvc;
//import java.util.Optional;
//
//
//@ExtendWith(SpringExtension.class)
//@SpringBootTest
//@AutoConfigureMockMvc
//
public class EnDictControllerTest {}
//{
//    @MockBean
//    private EnglishDictionaryService service;
//
//    @MockBean
//    private EnglishDictionaryRepository repository;
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Test
//    @DisplayName("GET /getAllENWords/1 - Found")
//    void testGetENWordByIdFound() throws Exception
//    {
//        WordAndTranslations mockWord = new WordAndTranslations(1, "kid", "laps", "", "", "", "");
//        doReturn(mockWord).when(repository).fetchSingleENWordById(1);
//    }
//        mockMvc.perform(get("/getAllENWords/{wordId}", 1))
//
//                .andExpect(status().isOk())
//                .andExpect(header().string(HttpHeaders.LOCATION, "/getAllENWords/1"))
//                .andExpect(jsonPath("$.id", is(1)))
//                .andExpect(jsonPath("$.originalWord", is("kid")))
//                .andExpect(jsonPath("$.firstTranslation", is("laps")))
//                .andExpect(jsonPath("$.secondTranslation", is("")))
//                .andExpect(jsonPath("$.thirdTranslation", is("")))
//                .andExpect(jsonPath("$.fourthTranslation", is("")))
//                .andExpect(jsonPath("$.fifthTranslation", is("")));
//
//    }

//    @Test
//    @DisplayName("GET /getAllENWords/1 - Not Found")
//    void testGetWordByIdNotFound() throws Exception {
//        doReturn(Optional.empty()).when(repository).fetchSingleENWordById(1);
//
//        mockMvc.perform(get("/getAllENWords/{wordId}", 1))
//                .andExpect(status().isNotFound());
//    }
