INSERT INTO `eedictionary` (`EE_word_id`, `EE_word`, `EE_word_EN_equivalent_1`, `EE_word_EN_equivalent_2`, `EE_word_EN_equivalent_3`, `EE_word_EN_equivalent_4`, `EE_word_EN_equivalent_5`) VALUES
	(46, 'naine', 'woman', '', '', '', ''),
	(47, 'kõlar', 'loudspeaker', 'speaker', '', '', ''),
	(48, 'auto', 'car', 'motorcar', 'automobile', '', ''),
	(49, 'laps', 'child', 'kid', 'infant', 'bairn', ''),
	(50, 'tugev', 'strong', '', '', '', ''),
	(51, 'kindel', 'strong', '', '', '', ''),
	(52, 'jõuline', 'strong', '', '', '', ''),
	(53, 'laud', 'board', '', '', '', ''),
	(54, 'paneel', 'board', '', '', '', ''),
	(55, 'plaat', 'board', '', '', '', ''),
	(56, 'juhatus', 'board', '', '', '', ''),
	(57, 'tahvel', 'board', '', '', '', '');

INSERT INTO `endictionary` (`EN_word_id`, `EN_word`, `EN_word_EE_equivalent_1`, `EN_word_EE_equivalent_2`, `EN_word_EE_equivalent_3`, `EN_word_EE_equivalent_4`, `EN_word_EE_equivalent_5`) VALUES
	(71, 'woman', 'naine', '', '', '', ''),
	(72, 'loudspeaker', 'kõlar', '', '', '', ''),
	(73, 'speaker', 'kõlar', '', '', '', ''),
	(74, 'car', 'auto', '', '', '', ''),
	(75, 'motorcar', 'auto', '', '', '', ''),
	(76, 'automobile', 'auto', '', '', '', ''),
	(77, 'child', 'laps', '', '', '', ''),
	(78, 'kid', 'laps', '', '', '', ''),
	(79, 'infant', 'laps', '', '', '', ''),
	(80, 'bairn', 'laps', '', '', '', ''),
	(81, 'strong', 'tugev', 'kindel', 'jõuline', '', ''),
	(82, 'board', 'laud', 'paneel', 'plaat', 'juhatus', 'tahvel');