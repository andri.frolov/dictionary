DROP TABLE IF EXISTS `eedictionary`;
DROP TABLE IF EXISTS `endictionary`;

CREATE DATABASE IF NOT EXISTS `dictionary`;
USE `dictionary`;

CREATE TABLE IF NOT EXISTS `eedictionary` (
  `EE_word_id` int(11) NOT NULL AUTO_INCREMENT,
  `EE_word` varchar(50) NOT NULL,
  `EE_word_EN_equivalent_1` varchar(50) NOT NULL,
  `EE_word_EN_equivalent_2` varchar(50) NOT NULL DEFAULT '',
  `EE_word_EN_equivalent_3` varchar(50) NOT NULL DEFAULT '',
  `EE_word_EN_equivalent_4` varchar(50) NOT NULL DEFAULT '',
  `EE_word_EN_equivalent_5` varchar(50) NOT NULL DEFAULT '',
  KEY `EE_word_id` (`EE_word_id`)
);

CREATE TABLE IF NOT EXISTS `endictionary` (
  `EN_word_id` int(11) NOT NULL AUTO_INCREMENT,
  `EN_word` varchar(50) NOT NULL,
  `EN_word_EE_equivalent_1` varchar(50) NOT NULL,
  `EN_word_EE_equivalent_2` varchar(50) NOT NULL DEFAULT '',
  `EN_word_EE_equivalent_3` varchar(50) NOT NULL DEFAULT '',
  `EN_word_EE_equivalent_4` varchar(50) NOT NULL DEFAULT '',
  `EN_word_EE_equivalent_5` varchar(50) NOT NULL DEFAULT '',
  KEY `EN_word_id` (`EN_word_id`)
);

