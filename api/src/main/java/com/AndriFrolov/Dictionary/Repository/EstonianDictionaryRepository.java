package com.AndriFrolov.Dictionary.Repository;

import com.AndriFrolov.Dictionary.Model.WordAndTranslations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EstonianDictionaryRepository
{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<WordAndTranslations> fetchAllEEWords()
    {
        return jdbcTemplate.query("Select * from eedictionary;",
            (dbRow, sequenceNumber) ->
            {
                return new WordAndTranslations(
                        dbRow.getInt("EE_word_id"),
                        dbRow.getString("EE_word"),
                        dbRow.getString("EE_word_EN_equivalent_1"),
                        dbRow.getString("EE_word_EN_equivalent_2"),
                        dbRow.getString("EE_word_EN_equivalent_3"),
                        dbRow.getString("EE_word_EN_equivalent_4"),
                        dbRow.getString("EE_word_EN_equivalent_5")
                );
            });
    }

    public WordAndTranslations fetchSingleEEWordById(int wordId)
    {
        List<WordAndTranslations> singleWord = jdbcTemplate.query("Select * from eedictionary where EE_word_id = ?;",
                new Object[] {wordId},
                (dbRow, sequenceNumber) ->
                {
                    return new WordAndTranslations(
                            dbRow.getInt("EE_word_id"),
                            dbRow.getString("EE_word"),
                            dbRow.getString("EE_word_EN_equivalent_1"),
                            dbRow.getString("EE_word_EN_equivalent_2"),
                            dbRow.getString("EE_word_EN_equivalent_3"),
                            dbRow.getString("EE_word_EN_equivalent_4"),
                            dbRow.getString("EE_word_EN_equivalent_5")
                    );
                }
        );

        if (singleWord.size() > 0) {
            return singleWord.get(0);
        } else {
            return null;
        }
    }

    public void addNewEEWord(WordAndTranslations newDictionaryEntry)
    {
        jdbcTemplate.update("insert into eedictionary (" +
                        "`EE_word`, " +
                        "`EE_word_EN_equivalent_1`, " +
                        "`EE_word_EN_equivalent_2`, " +
                        "`EE_word_EN_equivalent_3`, " +
                        "`EE_word_EN_equivalent_4`, " +
                        "`EE_word_EN_equivalent_5`) " +
                        "values (?, ?, ?, ?, ?, ?)",
                newDictionaryEntry.getOriginalWord(),
                newDictionaryEntry.getFirstTranslation(),
                newDictionaryEntry.getSecondTranslation(),
                newDictionaryEntry.getThirdTranslation(),
                newDictionaryEntry.getFourthTranslation(),
                newDictionaryEntry.getFifthTranslation()
        );

//        The following jdbcTemplate.updates make sure that when we create an Estonian word in our Estonian dictionary
//        we take its English equivalents, put them into the English dictionary as new words
//        and attach the original Estonian word to them as their Estonian equivalent.

        jdbcTemplate.update("insert into endictionary (`EN_word`, `EN_word_EE_equivalent_1`) values (?, ?)",
                newDictionaryEntry.getFirstTranslation(), newDictionaryEntry.getOriginalWord());

        if (!newDictionaryEntry.getSecondTranslation().isEmpty()) {
            jdbcTemplate.update("insert into endictionary (`EN_word`, `EN_word_EE_equivalent_1`) values (?, ?)",
                    newDictionaryEntry.getSecondTranslation(), newDictionaryEntry.getOriginalWord());
        }

        if (!newDictionaryEntry.getThirdTranslation().isEmpty()) {
            jdbcTemplate.update("insert into endictionary (`EN_word`, `EN_word_EE_equivalent_1`) values (?, ?)",
                    newDictionaryEntry.getThirdTranslation(), newDictionaryEntry.getOriginalWord());
        }

        if (!newDictionaryEntry.getFourthTranslation().isEmpty()) {
            jdbcTemplate.update("insert into endictionary (`EN_word`, `EN_word_EE_equivalent_1`) values (?, ?)",
                    newDictionaryEntry.getFourthTranslation(), newDictionaryEntry.getOriginalWord());
        }

        if (!newDictionaryEntry.getFifthTranslation().isEmpty()) {
            jdbcTemplate.update("insert into endictionary (`EN_word`, `EN_word_EE_equivalent_1`) values (?, ?)",
                    newDictionaryEntry.getFifthTranslation(), newDictionaryEntry.getOriginalWord());
        }
    }

    public void editExistingEEWord(WordAndTranslations existingWord) {
        jdbcTemplate.update("update eedictionary set " +
                    "`EE_word` = ?, " +
                    "`EE_word_EN_equivalent_1` = ?, " +
                    "`EE_word_EN_equivalent_2` = ?, " +
                    "`EE_word_EN_equivalent_3` = ?, " +
                    "`EE_word_EN_equivalent_4` = ?, " +
                    "`EE_word_EN_equivalent_5` = ? " +
                    "where `EE_word_id` = ?",
                existingWord.getOriginalWord(),
                existingWord.getFirstTranslation(),
                existingWord.getSecondTranslation(),
                existingWord.getThirdTranslation(),
                existingWord.getFourthTranslation(),
                existingWord.getFifthTranslation(),
                existingWord.getId()
        );
    }

    public void deleteEEWord(int wordId){
        jdbcTemplate.update("delete from eedictionary where `EE_word_id` = ?", wordId);
    }
}