package com.AndriFrolov.Dictionary.Repository;

import com.AndriFrolov.Dictionary.Model.WordAndTranslations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EnglishDictionaryRepository
{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<WordAndTranslations> fetchAllENWords()
    {
        return jdbcTemplate.query("Select * from endictionary;",
            (dbRow, sequenceNumber) ->
            {
                return new WordAndTranslations(
                        dbRow.getInt("EN_word_id"),
                        dbRow.getString("EN_word"),
                        dbRow.getString("EN_word_EE_equivalent_1"),
                        dbRow.getString("EN_word_EE_equivalent_2"),
                        dbRow.getString("EN_word_EE_equivalent_3"),
                        dbRow.getString("EN_word_EE_equivalent_4"),
                        dbRow.getString("EN_word_EE_equivalent_5")
                );
            });
    }

    public WordAndTranslations fetchSingleENWordById(int wordId)
    {
        List<WordAndTranslations> singleWord = jdbcTemplate.query("Select * from endictionary where EN_word_id = ?;",
                new Object[] {wordId},
                (dbRow, sequenceNumber) ->
                {
                    return new WordAndTranslations(
                            dbRow.getInt("EN_word_id"),
                            dbRow.getString("EN_word"),
                            dbRow.getString("EN_word_EE_equivalent_1"),
                            dbRow.getString("EN_word_EE_equivalent_2"),
                            dbRow.getString("EN_word_EE_equivalent_3"),
                            dbRow.getString("EN_word_EE_equivalent_4"),
                            dbRow.getString("EN_word_EE_equivalent_5")
                    );
                }
        );

        if (singleWord.size() > 0) {
            return singleWord.get(0);
        } else {
            return null;
        }
    }


    public void addNewENWord(WordAndTranslations newDictionaryEntry)
    {
        jdbcTemplate.update("insert into endictionary (" +
                        "`EN_word`, " +
                        "`EN_word_EE_equivalent_1`, " +
                        "`EN_word_EE_equivalent_2`, " +
                        "`EN_word_EE_equivalent_3`, " +
                        "`EN_word_EE_equivalent_4`, " +
                        "`EN_word_EE_equivalent_5`) " +
                        "values (?, ?, ?, ?, ?, ?)",
                newDictionaryEntry.getOriginalWord(),
                newDictionaryEntry.getFirstTranslation(),
                newDictionaryEntry.getSecondTranslation(),
                newDictionaryEntry.getThirdTranslation(),
                newDictionaryEntry.getFourthTranslation(),
                newDictionaryEntry.getFifthTranslation()
        );

//        The following jdbcTemplate.updates make sure that when we create an English word in our English dictionary
//        we take its Estonian equivalents, put them into the Estonian dictionary as new words
//        and attach the original English word to them as their English equivalent.

        jdbcTemplate.update("insert into eedictionary (`EE_word`, `EE_word_EN_equivalent_1`) values (?, ?)",
                newDictionaryEntry.getFirstTranslation(), newDictionaryEntry.getOriginalWord());

        if (!newDictionaryEntry.getSecondTranslation().isEmpty()) {
            jdbcTemplate.update("insert into eedictionary (`EE_word`, `EE_word_EN_equivalent_1`) values (?, ?)",
                    newDictionaryEntry.getSecondTranslation(), newDictionaryEntry.getOriginalWord());
        }

        if (!newDictionaryEntry.getThirdTranslation().isEmpty()) {
            jdbcTemplate.update("insert into eedictionary (`EE_word`, `EE_word_EN_equivalent_1`) values (?, ?)",
                    newDictionaryEntry.getThirdTranslation(), newDictionaryEntry.getOriginalWord());
        }

        if (!newDictionaryEntry.getFourthTranslation().isEmpty()) {
            jdbcTemplate.update("insert into eedictionary (`EE_word`, `EE_word_EN_equivalent_1`) values (?, ?)",
                    newDictionaryEntry.getFourthTranslation(), newDictionaryEntry.getOriginalWord());
        }

        if (!newDictionaryEntry.getFifthTranslation().isEmpty()) {
            jdbcTemplate.update("insert into eedictionary (`EE_word`, `EE_word_EN_equivalent_1`) values (?, ?)",
                    newDictionaryEntry.getFifthTranslation(), newDictionaryEntry.getOriginalWord());
        }
    }

    public void editExistingENWord(WordAndTranslations existingWord) {
        jdbcTemplate.update("update endictionary set " +
                        "`EN_word` = ?, " +
                        "`EN_word_EE_equivalent_1` = ?, " +
                        "`EN_word_EE_equivalent_2` = ?, " +
                        "`EN_word_EE_equivalent_3` = ?, " +
                        "`EN_word_EE_equivalent_4` = ?, " +
                        "`EN_word_EE_equivalent_5` = ? " +
                        "where `EN_word_id` = ?",
                existingWord.getOriginalWord(),
                existingWord.getFirstTranslation(),
                existingWord.getSecondTranslation(),
                existingWord.getThirdTranslation(),
                existingWord.getFourthTranslation(),
                existingWord.getFifthTranslation(),
                existingWord.getId()
        );
    }

    public void deleteENWord(int wordId){
        jdbcTemplate.update("delete from endictionary where `EN_word_id` = ?", wordId);
    }
}
