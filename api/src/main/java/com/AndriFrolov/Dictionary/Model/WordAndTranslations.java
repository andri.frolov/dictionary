package com.AndriFrolov.Dictionary.Model;

public class WordAndTranslations
{
    private int id;
    private String originalWord;
    private String firstTranslation;
    private String secondTranslation;
    private String thirdTranslation;
    private String fourthTranslation;
    private String fifthTranslation;

    public WordAndTranslations(int id, String originalWord, String firstTranslation, String secondTranslation, String thirdTranslation, String fourthTranslation, String fifthTranslation)
    {
        this.id = id;
        this.originalWord = originalWord;
        this.firstTranslation = firstTranslation;
        this.secondTranslation = secondTranslation;
        this.thirdTranslation = thirdTranslation;
        this.fourthTranslation = fourthTranslation;
        this.fifthTranslation = fifthTranslation;
    }

    public int getId()
    {
        return id;
    }

    public String getOriginalWord()
    {
        return originalWord;
    }

    public String getFirstTranslation()
    {
        return firstTranslation;
    }

    public String getSecondTranslation()
    {
        return secondTranslation;
    }

    public String getThirdTranslation()
    {
        return thirdTranslation;
    }

    public String getFourthTranslation()
    {
        return fourthTranslation;
    }

    public String getFifthTranslation()
    {
        return fifthTranslation;
    }

    @Override
    public String toString()
    {
        return "WordAndTranslations{" +
                "id=" + id +
                ", originalWord='" + originalWord + '\'' +
                ", firstTranslation='" + firstTranslation + '\'' +
                ", secondTranslation='" + secondTranslation + '\'' +
                ", thirdTranslation='" + thirdTranslation + '\'' +
                ", fourthTranslation='" + fourthTranslation + '\'' +
                ", fifthTranslation='" + fifthTranslation + '\'' +
                '}';
    }
}
