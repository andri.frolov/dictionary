package com.AndriFrolov.Dictionary.Service;

import com.AndriFrolov.Dictionary.Model.WordAndTranslations;
import com.AndriFrolov.Dictionary.Repository.EnglishDictionaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnglishDictionaryService
{
    @Autowired
    private EnglishDictionaryRepository englishDictionaryRepository;

    public void addOrEditENWord(WordAndTranslations dictionaryEntry) {
        if (dictionaryEntry.getId() > 0) {
            System.out.println("Editing existing English word...");
            englishDictionaryRepository.editExistingENWord(dictionaryEntry);
        } else {
            System.out.println("Adding new English word...");
            englishDictionaryRepository.addNewENWord(dictionaryEntry);
        }
    }
}
