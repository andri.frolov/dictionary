package com.AndriFrolov.Dictionary.Service;

import com.AndriFrolov.Dictionary.Model.WordAndTranslations;
import com.AndriFrolov.Dictionary.Repository.EstonianDictionaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EstonianDictionaryService
{
    @Autowired
    private EstonianDictionaryRepository estonianDictionaryRepository;

    public void addOrEditEEWord(WordAndTranslations dictionaryEntry) {
        if (dictionaryEntry.getId() > 0) {
            System.out.println("Editing existing Estonian word...");
            estonianDictionaryRepository.editExistingEEWord(dictionaryEntry);
        } else {
            System.out.println("Adding new Estonian word...");
            estonianDictionaryRepository.addNewEEWord(dictionaryEntry);
        }
    }
}
