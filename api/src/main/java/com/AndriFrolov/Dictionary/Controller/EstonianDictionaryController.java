package com.AndriFrolov.Dictionary.Controller;

import com.AndriFrolov.Dictionary.Model.WordAndTranslations;
import com.AndriFrolov.Dictionary.Repository.EstonianDictionaryRepository;
import com.AndriFrolov.Dictionary.Service.EstonianDictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/EEdictionary")
@CrossOrigin
public class EstonianDictionaryController
{
    @Autowired
    private EstonianDictionaryRepository estonianDictionaryRepository;

    @Autowired
    private EstonianDictionaryService estonianDictionaryService;

    @GetMapping("/getAllEEWords")
    public List<WordAndTranslations> getAllEEWords()
    {
        System.out.println("Getting all Estonian words");
        return estonianDictionaryRepository.fetchAllEEWords();
    }

    @RequestMapping(value = "/addOrEditEEWord",
            method = {RequestMethod.POST, RequestMethod.PUT})
    public void addOrEditWord(@RequestBody WordAndTranslations dictionaryEntry)
    {
        System.out.println("Adding a new Estonian word or editing an existing one...");
        estonianDictionaryService.addOrEditEEWord(dictionaryEntry);
    }

    @GetMapping("/getAllEEWords/{wordId}")
    public WordAndTranslations getSingleWordById(@PathVariable int wordId) {
        System.out.println("Getting an Estonian word @id: " + wordId);
        return estonianDictionaryRepository.fetchSingleEEWordById(wordId);
    }

    @DeleteMapping("/deleteEEWord/{wordId}")
    public void deleteEEWord(@PathVariable int wordId){
        System.out.println("Deleting Estonian word @id: " + wordId);
        estonianDictionaryRepository.deleteEEWord(wordId);
    }
}