package com.AndriFrolov.Dictionary.Controller;

import com.AndriFrolov.Dictionary.Model.WordAndTranslations;
import com.AndriFrolov.Dictionary.Repository.EnglishDictionaryRepository;
import com.AndriFrolov.Dictionary.Service.EnglishDictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ENdictionary")
@CrossOrigin
public class EnglishDictionaryController
{
    @Autowired
    private EnglishDictionaryRepository englishDictionaryRepository;

    @Autowired
    private EnglishDictionaryService englishDictionaryService;

    @GetMapping("/getAllENWords")
    public List<WordAndTranslations> getAllEEWords()
    {
        System.out.println("Getting all English words");
        return englishDictionaryRepository.fetchAllENWords();
    }

    @GetMapping("/getAllENWords/{wordId}")
    public WordAndTranslations getSingleWordById(@PathVariable int wordId) {
        System.out.println("Getting an Estonian word @id: " + wordId);
        return englishDictionaryRepository.fetchSingleENWordById(wordId);
    }

    @RequestMapping(value = "/addOrEditENWord",
            method = {RequestMethod.POST, RequestMethod.PUT})
    public void addOrEditWord(@RequestBody WordAndTranslations dictionaryEntry)
    {
        System.out.println("Adding a new English word or editing an existing one...");
        englishDictionaryService.addOrEditENWord(dictionaryEntry);
    }

    @DeleteMapping("/deleteENWord/{wordId}")
    public void deleteEEWord(@PathVariable int wordId)
    {
        System.out.println("Deleting English word @id: " + wordId);
        englishDictionaryRepository.deleteENWord(wordId);
    }
}
