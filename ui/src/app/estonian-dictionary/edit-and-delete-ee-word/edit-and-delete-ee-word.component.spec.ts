import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAndDeleteWordComponent } from './edit-and-delete-ee-word.component';

describe('EditAndDeleteWordComponent', () => {
  let component: EditAndDeleteWordComponent;
  let fixture: ComponentFixture<EditAndDeleteWordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAndDeleteWordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAndDeleteWordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
