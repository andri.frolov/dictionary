import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEeWordComponent } from './add-ee-word.component';

describe('AddEnWordComponent', () => {
  let component: AddEeWordComponent;
  let fixture: ComponentFixture<AddEeWordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEeWordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEeWordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
