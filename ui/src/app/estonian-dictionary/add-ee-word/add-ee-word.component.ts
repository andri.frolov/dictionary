import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Word } from 'src/app/models/word-model';
import { EstonianDictionaryService } from 'src/app/services/ee-dictionary-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-ee-word.component',
  templateUrl: './add-ee-word.component.html',
  styleUrls: ['./add-ee-word.component.css']
})

export class AddEeWordComponent implements OnInit {
  addWordForm: FormGroup;
  word: Word;
  errorMessage: string;

  constructor(
    private formBuilder: FormBuilder,
    private eeDictService: EstonianDictionaryService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.addWordForm = this.formBuilder.group({
      originalWord: ['', [Validators.required, Validators.maxLength(50)]],
      firstTranslation: ['', [Validators.required, Validators.maxLength(50)]],
      secondTranslation: ['', [Validators.maxLength(50)]],
      thirdTranslation: ['', [Validators.maxLength(50)]],
      fourthTranslation: ['', [Validators.maxLength(50)]],
      fifthTranslation: ['', [Validators.maxLength(50)]]
    });
  }

  populateTestData(): void {
    this.addWordForm.patchValue({
      originalWord: 'laps',
      firstTranslation: 'kid100',
      secondTranslation: 'kid200',
      thirdTranslation: 'kid300',
      fourthTranslation: 'kid400',
      fifthTranslation: 'kid500'
    })
  }
  
  saveEEWord(): void {
    console.log('saveEEWord()')
    if (this.addWordForm.valid) {
      if(this.addWordForm.dirty) {
        const word = { ...this.word, ...this.addWordForm.value }

        this.eeDictService.createEEWord(word)
          .subscribe({
            next: () => this.onSaveComplete(),
            error: err => this.errorMessage = err 
        })
      }
    }
  }

  onSaveComplete(): void {
    this.addWordForm.reset();
    this.router.navigate(['/EEdict'])
  }
}
