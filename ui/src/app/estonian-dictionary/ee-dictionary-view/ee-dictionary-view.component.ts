import { Component, OnInit } from '@angular/core';
import { EstonianDictionaryService } from 'src/app/services/ee-dictionary-service';
import { IWord } from 'src/app/models/word-model';

@Component({
  selector: 'app-ee-dictionary-view',
  templateUrl: './ee-dictionary-view.component.html',
  styleUrls: ['./ee-dictionary-view.component.css']
})

export class EEDictionaryViewComponent implements OnInit {
  words: IWord[] = [];
  errorMessage: string;
  filteredWords: IWord[];

  _wordFilter: string;
  get wordFilter(): string {
    return this._wordFilter;
  }

  set wordFilter(value: string) {
    this._wordFilter = value;
    this.filteredWords = this.wordFilter ? this.performFilter(this.wordFilter) : this.words;
  }

  performFilter(filterBy: string): IWord[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.words.filter((word: IWord) => 
      word.originalWord.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }

  constructor(private estonianDictionaryService: EstonianDictionaryService) {
  }

  ngOnInit(): void {
    this.estonianDictionaryService.getEEWords().subscribe({
      next: words => {
        this.words = words;
        this.filteredWords = words;
      },
      error: err => this.errorMessage = err
    });
  }
}
