import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EEDictionaryViewComponent } from './ee-dictionary-view.component';

describe('EEDictionaryViewComponent', () => {
  let component: EEDictionaryViewComponent;
  let fixture: ComponentFixture<EEDictionaryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EEDictionaryViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EEDictionaryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
