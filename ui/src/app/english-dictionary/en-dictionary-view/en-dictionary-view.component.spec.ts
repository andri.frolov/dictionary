import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ENDictionaryViewComponent } from './en-dictionary-view.component';

describe('EnDictionaryViewComponent', () => {
  let component: ENDictionaryViewComponent;
  let fixture: ComponentFixture<ENDictionaryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ENDictionaryViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ENDictionaryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
