import { Component, OnInit } from '@angular/core';
import { EnglishDictionaryService } from 'src/app/services/en-dictionary-service';
import { IWord } from 'src/app/models/word-model';

@Component({
  selector: 'app-en-dictionary-view',
  templateUrl: './en-dictionary-view.component.html',
  styleUrls: ['./en-dictionary-view.component.css']
})

export class ENDictionaryViewComponent implements OnInit {
  words: IWord[] = [];
  errorMessage: string;
  filteredWords: IWord[];

  _wordFilter: string;
  get wordFilter(): string {
    return this._wordFilter;
  }

  set wordFilter(value: string) {
    this._wordFilter = value;
    this.filteredWords = this.wordFilter ? this.performFilter(this.wordFilter) : this.words;
  }

  performFilter(filterBy: string): IWord[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.words.filter((word: IWord) => 
      word.originalWord.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }

  constructor(private estonianDictionaryService: EnglishDictionaryService) {
  }

  ngOnInit(): void {
    this.estonianDictionaryService.getENWords().subscribe({
      next: words => {
        this.words = words;
        this.filteredWords = words;
      },
      error: err => this.errorMessage = err
    });
  }
}
