import { Component, OnInit, Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Word } from 'src/app/models/word-model';
import { EnglishDictionaryService } from 'src/app/services/en-dictionary-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-en-word',
  templateUrl: './add-en-word.component.html',
  styleUrls: ['./add-en-word.component.css']
})
export class AddEnWordComponent implements OnInit {
  addWordForm: FormGroup;
  word: Word;
  errorMessage: string;

  constructor(
    private formBuilder: FormBuilder,
    private enDictService: EnglishDictionaryService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.addWordForm = this.formBuilder.group({
      originalWord: ['', [Validators.required, Validators.maxLength(50)]],
      firstTranslation: ['', [Validators.required, Validators.maxLength(50)]],
      secondTranslation: ['', [Validators.maxLength(50)]],
      thirdTranslation: ['', [Validators.maxLength(50)]],
      fourthTranslation: ['', [Validators.maxLength(50)]],
      fifthTranslation: ['', [Validators.maxLength(50)]]
    });
  }

  saveENWord(): void {
    if (this.addWordForm.valid) {
      if(this.addWordForm.dirty) {
        const word = { ...this.word, ...this.addWordForm.value }

        this.enDictService.createENWord(word)
          .subscribe({
            next: () => this.onSaveComplete(),
            error: err => this.errorMessage = err 
        })
      }
    }
  }

  onSaveComplete(): void {
    this.addWordForm.reset();
    this.router.navigate(['/ENdict'])
  }
}
