import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEnWordComponent } from './add-en-word.component';

describe('AddEnWordComponent', () => {
  let component: AddEnWordComponent;
  let fixture: ComponentFixture<AddEnWordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEnWordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEnWordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
