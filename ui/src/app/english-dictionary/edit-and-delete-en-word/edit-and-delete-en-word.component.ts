import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Word } from 'src/app/models/word-model';
import { EnglishDictionaryService } from 'src/app/services/en-dictionary-service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-and-delete-en-word',
  templateUrl: './edit-and-delete-en-word.component.html',
  styleUrls: ['./edit-and-delete-en-word.component.css']
})
export class EditAndDeleteEnWordComponent implements OnInit {
  editWordForm: FormGroup;
  errorMessage: string;
  private sub: Subscription;
  word: Word;
  
  constructor(
    private fb: FormBuilder,
    private enDictService: EnglishDictionaryService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.editWordForm = this.fb.group({
      originalWord: ['', [Validators.required, Validators.maxLength(50)]],
      firstTranslation: ['', [Validators.required, Validators.maxLength(50)]],
      secondTranslation: ['', [Validators.maxLength(50)]],
      thirdTranslation: ['', [Validators.maxLength(50)]],
      fourthTranslation: ['', [Validators.maxLength(50)]],
      fifthTranslation: ['', [Validators.maxLength(50)]]
  })

  this.sub = this.route.paramMap.subscribe(
    params => {
      const id = +params.get('id');
      this.getWordForForm(id);
    }
  )}

  getWordForForm(id: number): void {
    this.enDictService.getSingleENWordById(id).subscribe({
      next: (word: Word) => this.fillWordForm(word),
      error: err => this.errorMessage = err
    })
  }

  fillWordForm(word: Word): void {
    this.word = word;
    this.editWordForm.patchValue({
      originalWord: this.word.originalWord,
      firstTranslation: this.word.firstTranslation,
      secondTranslation: this.word.secondTranslation,
      thirdTranslation: this.word.thirdTranslation,
      fourthTranslation: this.word.fourthTranslation,
      fifthTranslation: this.word.fifthTranslation
    })
  }

  editENWord(): void {
    if (this.editWordForm.valid) {
      if(this.editWordForm.dirty) {
        const word = { ...this.word, ...this.editWordForm.value }

        this.enDictService.updateENWord(word)
          .subscribe({
            next: () => this.onSaveComplete(),
            error: err => this.errorMessage = err 
        })
      }
    }
  }

  onSaveComplete(): void {
    this.editWordForm.reset();
    this.returnToDictionary();
  }

  deleteENWord(): void{
    if (confirm(`Kas oled kindel, et soovid kustutada sõna "${this.word.originalWord}"?`)) {
      this.enDictService.deleteWord(+this.word.id).subscribe({
        next: () => this.returnToDictionary(),
        error: err => this.errorMessage = err
      })
    }
  }

  returnToDictionary() {
    this.router.navigate(['/ENdict']);
  }
}
