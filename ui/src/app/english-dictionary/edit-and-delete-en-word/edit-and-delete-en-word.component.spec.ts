import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAndDeleteEnWordComponent } from './edit-and-delete-en-word.component';

describe('EditAndDeleteEnWordComponent', () => {
  let component: EditAndDeleteEnWordComponent;
  let fixture: ComponentFixture<EditAndDeleteEnWordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAndDeleteEnWordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAndDeleteEnWordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
