import { Injectable } from "@angular/core";
import { IWord, Word } from '../models/word-model';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class EstonianDictionaryService {
    public baseUrl = "http://localhost:8080/EEdictionary"
    words: IWord[] = [];

    constructor(private http: HttpClient) { }

    getSingleEEWordById(incomingId: number): Observable<IWord> {
        console.log("incomingId is: " + incomingId)
        return this.http.get<IWord>(`${this.baseUrl}/getAllEEWords/${incomingId}`);
    }

    getEEWords(): Observable<IWord[]> {
        return this.http.get<IWord[]>(`${this.baseUrl}/getAllEEWords`).
            pipe(catchError(this.handleError));
    }

    createEEWord(eeWord: Word): Observable<Word> {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        })
        const url = `${this.baseUrl}/addOrEditEEWord`;
        return this.http.post<Word>(
            url, eeWord, { headers: headers })
    }

    updateEEWord(updatedEEWord: Word): Observable<Word> {
        const headers = new HttpHeaders({ 
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        });

        const url = `${this.baseUrl}/addOrEditEEWord`;
        return this.http.put<Word>(
            url, updatedEEWord, { headers: headers})
            .pipe(
                tap(() => console.log('updateEEWord: ' + updatedEEWord.id)),
                map(() => updatedEEWord),
                catchError(this.handleError)
        );
    }

    deleteWord(wordId: number): Observable<{}> {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        const url = `${this.baseUrl}/deleteEEWord/${wordId}`
        return this.http.delete<Word>(url, { headers: headers });
    }

    private handleError(err: HttpErrorResponse) {
        let errorMessage = '';
        if (err.error instanceof ErrorEvent) {
            errorMessage = `An error occurred: ${err.error.message}`;
        } else {
            errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
        }
        console.error(errorMessage);
        return throwError(errorMessage);
    }
}

