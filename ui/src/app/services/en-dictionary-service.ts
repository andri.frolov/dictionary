import { Injectable } from "@angular/core";
import { IWord, Word } from '../models/word-model';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class EnglishDictionaryService {
  public baseUrl = 'http://localhost:8080/ENdictionary';
  words: IWord[];

  constructor(private http: HttpClient) {}

  getENWords() {
    return this.http.get<IWord[]>(`${this.baseUrl}/getAllENWords`).
        pipe(catchError(this.handleError))
  }

  getSingleENWordById(incomingId: number): Observable<IWord> {
    return this.http.get<IWord>(`${this.baseUrl}/getAllENWords/${incomingId}`);
  }
 
  createENWord(enWord: Word): Observable<Word> {
    const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    })
    const url = `${this.baseUrl}/addOrEditENWord`;
    return this.http.post<Word>(
        url, enWord, {headers: headers})
  }

  updateENWord(updatedENWord: Word): Observable<Word> {
    const headers = new HttpHeaders({ 
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    });

    const url = `${this.baseUrl}/addOrEditENWord`;
    return this.http.put<Word>(
        url, updatedENWord, { headers: headers})
        .pipe(
            tap(() => console.log('updateEEWord: ' + updatedENWord.id)),
            map(() => updatedENWord),
            catchError(this.handleError)
    );
}

  deleteWord(wordId: number): Observable<{}> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.baseUrl}/deleteENWord/${wordId}`;
    return this.http.delete<Word>(url, {headers: headers});
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
        errorMessage = `An error occurred: ${err.error.message}`;
    } else {
        errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
}



