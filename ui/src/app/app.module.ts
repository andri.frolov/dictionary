import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { EEDictionaryViewComponent } from './estonian-dictionary/ee-dictionary-view/ee-dictionary-view.component';
import { ENDictionaryViewComponent } from './english-dictionary/en-dictionary-view/en-dictionary-view.component';
import { AddEeWordComponent } from './estonian-dictionary/add-ee-word/add-ee-word.component';
import { AddEnWordComponent } from './english-dictionary/add-en-word/add-en-word.component';
import { EditAndDeleteWordComponent } from './estonian-dictionary/edit-and-delete-ee-word/edit-and-delete-ee-word.component';
import { EditAndDeleteEnWordComponent } from './english-dictionary/edit-and-delete-en-word/edit-and-delete-en-word.component';
import { WordsortPipe } from './pipes/wordsort.pipe';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    EEDictionaryViewComponent,
    ENDictionaryViewComponent,
    AddEeWordComponent,
    AddEnWordComponent,
    EditAndDeleteWordComponent,
    EditAndDeleteEnWordComponent,
    NavbarComponent,
    WordsortPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'EEdict', component: EEDictionaryViewComponent },
      { path: 'ENdict', component: ENDictionaryViewComponent},
      { path: 'addNewEEWord', component: AddEeWordComponent },
      { path: 'addNewENWord', component: AddEnWordComponent},
      { path: 'editEEWord/:id', component: EditAndDeleteWordComponent },
      { path: 'editENWord/:id', component: EditAndDeleteEnWordComponent},
      { path: '', redirectTo: 'EEdict', pathMatch: 'full' },
      { path: '**', redirectTo: 'EEdict', pathMatch: 'full' }
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
