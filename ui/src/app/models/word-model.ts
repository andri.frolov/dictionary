export interface IWord {
    id: string;
    originalWord: string;
    firstTranslation: string;
    secondTranslation: string;
    thirdTranslation: string;
    fourthTranslation: string;
    fifthTranslation: string;
}

export class Word implements IWord {
    constructor(
        public id: string,
        public originalWord: string,
        public firstTranslation: string,
        public secondTranslation: string,
        public thirdTranslation: string,
        public fourthTranslation: string,
        public fifthTranslation: string
    ) {}
}