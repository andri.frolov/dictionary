import { Pipe, PipeTransform } from '@angular/core';
import { IWord } from '../models/word-model';

@Pipe({
  name: 'wordsort'
})
export class WordsortPipe implements PipeTransform {

  transform(words: IWord[], searchTerm: string): IWord[] {
    return words.sort((a, b) => {
      let aWord: string = a.originalWord.toLowerCase();
      let bWord: string = b.originalWord.toLowerCase();
      return aWord < bWord ? -1 : (aWord > bWord ? 1 : 0);
    });
  }
}
