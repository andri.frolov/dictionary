GitLabi repo: https://gitlab.com/andri.frolov/dictionary

Rakenduse kohalikus masinas töölesaamiseks tuleb alla laadida vaid "api" kaust.
"api" kaust sisaldab backendi - Springi põhjal ja Gradle'i abiga ehitatud Java REST rakendust - ja Angularis ehitatud frontendi production faile ( kaustas src/main/resources/static).

Rakenduse ülesseadmiseks:

1. Tõmba alla "api" kaust.

2. Võta see IDE-s lahti (mina kasutasin Intellij-d) ja lase Gradle'il rakendus üles ehitada (build). Intellij pakkus mulle seda automaatselt.

3. Palun vaata, et IDE settingutes oleks Gradle JVM valitud. Java 14 sobib hästi.
	(Intellij-s on see: file (ülalt vasakult) -> settings -> Build, Execution, Deployment -> Build Tools -> Gradle -> Gradle JVM )

4. Loo enda valitud andmebaasisüsteemis uus andmebaas nimega "dictionary". (Mina kasutasin MariaDB-d ja HeidiSQL-i).

5. Palun kontrolli Java rakenduse failis src/main/resources/application.properties oma andmebaasi rakenduse porti (spring.datasource.url real) ning kasutajanime ja parooli.
	Asenda 3306 selle pordiga, mida sinu andmebaasisüsteem rakendab. Vaheta ka failis olev kasutajanimi ja parool oma andmebaasisüsteemi sisselogimise omadega.

6. Käivita rakendus Gradle'i bootrun käsuga (Gradle -> Dictionary -> Tasks -> application -> bootRun)

7. Ava brauseris leht https://dictionary-demo.herokuapp.com/ . Mina kasutasin Chrome'i.

8. Kui kõik läks hästi, siis peaksid nägema lehte koos eestikeelsete sõnadega. Sellisel juhul on leht täisfunktsionaalne.